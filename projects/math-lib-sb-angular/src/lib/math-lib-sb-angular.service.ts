import { Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class MathLibSbAngularService {
  getFloatNumberWithDot(value: string) {
    if (value && value.includes(',')) {
      return parseFloat(value.replace(',', '.'));
    } else {
      return parseFloat(value);
    }
  }

  getFloatNumberWithComma(value: string) {
    if (value && value.includes('.')) {
      return value.replace('.', ',');
    } else {
      return value;
    }
  }

  truncateNumberWithoutRounding(value: string, numberOfDecimals = 2) {
    const numberPartials = value.split('.');

    if (numberPartials.length > 1) {
      const decimals = numberPartials[1].substring(0, numberOfDecimals);

      return [numberPartials[0], decimals].join('.');
    } else {
      return value;
    }
  }
}
