import { NgModule } from '@angular/core';
import { MathLibSbAngularComponent } from './math-lib-sb-angular.component';
import { MathLibSbAngularService } from './math-lib-sb-angular.service';



@NgModule({
  declarations: [
    MathLibSbAngularComponent
  ],
  imports: [
  ],
  exports: [
    MathLibSbAngularComponent
  ],
  providers: [
    MathLibSbAngularService
  ]
})
export class MathLibSbAngularModule { }
