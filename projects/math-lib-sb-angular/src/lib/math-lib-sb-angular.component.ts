import { Component } from '@angular/core';

@Component({
  selector: 'lib-math-lib-sb-angular',
  template: ``,
  styles: [
  ]
})
export class MathLibSbAngularComponent {
  getFloatNumberWithDot(value: string) {
    if (value && value.includes(',')) {
      return parseFloat(value.replace(',', '.'));
    } else {
      return parseFloat(value);
    }
  }

  getFloatNumberWithComma(value: string) {
    if (value && value.includes('.')) {
      return value.replace('.', ',');
    } else {
      return value;
    }
  }

  truncateNumberWithoutRounding(value: string, numberOfDecimals = 2) {
    const numberPartials = value.split('.');

    if (numberPartials.length > 1) {
      const decimals = numberPartials[1].substring(0, numberOfDecimals);

      return [numberPartials[0], decimals].join('.');
    } else {
      return value;
    }
  }
}
